#
# IQ Hive base nginx install
#
FROM iqhive/gentoo-patched
MAINTAINER Relihan Myburgh <rmyburgh@iqhive.com>
LABEL description="Latest nginx"

RUN echo "www-servers/nginx "nginx_modules_http_slowfs_cache"" > /etc/portage/package.use/nginx; emerge -v www-servers/nginx; rm /usr/portage/distfiles/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log; ln -sf /dev/stderr /var/log/nginx/error.log
COPY export/ /
VOLUME /var/www/localhost/htdocs
EXPOSE 80
ENTRYPOINT /usr/sbin/nginx
